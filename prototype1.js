/**
 * Created by s25854 on 12/10/2016.
 */
//prototype1.js - prototye patterns sub-program
var TelsaModelS = function () {
    this.numWheels = 4;
    this.manufacturer = "telsa";
    this.make = "ModelS";
}
TelsaModelS.prototype = function(){
    var go = function(){
        console.log("Go");
    };
    var stop = function(){
        console.log("stop");
    };
    return {
        pressBrakePedal: stop,
        jamBrake: stop,
        pressGasPedal: go
    }
}();
module.exports = TelsaModelS;