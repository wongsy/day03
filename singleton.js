/**
 * Created by s25854 on 12/10/2016.
 */
var singleton = require('./a-singleton');

console.log(singleton.someProperty);
console.log(singleton.toggleZ(1));
console.log(singleton.toggleZ(2));