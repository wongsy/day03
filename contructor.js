/**
 * Created by s25854 on 12/10/2016.
 */
//contruct object Method 1
var newObject = {};
console.log(newObject);
var newObject2 = {name: "ken"};
console.log(newObject2);

//method 2
var newObject3 = Object.create(Object.prototype);
newObject3.name = "Alex";
newObject3.age = 18;
console.log(newObject3);

//method 3
var newObject4 = new Object();
newObject4.name = "Sandy";
console.log(newObject4);

console.log(newObject4.name);
console.log(newObject4['name']);

//to manipulate the attribute of the newly created object "name"
Object.defineProperty(newObject4, "name", {
    value: "Lin",
    writable: true,
    enumerable: false,
    configurable: true
});
console.log(newObject4['name']);
console.log(newObject4.name);
newObject4.name = "Jack";
console.log(newObject4['name']);

var o = {};
Object.defineProperty(o, "a", {value: '1', enumerable: true});
Object.defineProperty(o, "b", {value: '2', enumerable: false});
Object.defineProperty(o, "c", {value: '3', enumerable: true, configurable: true});

console.log(o);
console.log(o.b);
delete(o.a);  //not deleted as configurable defaulted to false
delete(o.c); //deleted as configurable set to true
console.log(o);



