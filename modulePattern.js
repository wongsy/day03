/**
 * Created by s25854 on 12/10/2016.
 */
//modulePattern.js
var a = require('./a.js');
var b = require('./b.js');
var c = require('./x/c.js');
// console.log(a.loaded);
console.log(a.bWasLoaded);
console.log(a.age);
console.log(a.drive());
console.log("----------");
console.log(b.aWasLoaded);
console.log(b.loaded);
console.log(b.run());