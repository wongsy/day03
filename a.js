/**
 * Created by s25854 on 12/10/2016.
 */
//a.js
var b =require('./b');  //take in that source code to be used in this js file to use some function

//make 4 variables and 1 function as public
//exports.loaded = true;   //make a.js public
var x = 9;               //local and private

module.exports = {      //make it public
    bWasLoaded: b.loaded,
    loaded2: true,
    age: 18,
    drive: function () {
        console.log("I am driving");
        return "driving";
    }
}



