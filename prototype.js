/**
 * Created by s25854 on 12/10/2016.
 */
//prototype.js - main program
var TelsaModelS = require('./prototype1.js');
var x = new TelsaModelS();
x.pressGasPedal();  //prints go
x.pressBrakePedal(); //prints stop
x.jamBrake();  //prints stop
console.log(x.numWheels);  //prints 4