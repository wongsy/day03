/**
 * Created by s25854 on 12/10/2016.
 */
//b.js

var a =require('./a');  //take in that source code to be used in this js file to use some function
exports.loadedInstance = false;

module.exports = {      //make it public
    aWasLoaded: a.loaded,
    loaded : true,
    run: function(){
        console.log("Running");
        return "Running";
    }
}