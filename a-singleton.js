/**
 * Created by s25854 on 12/10/2016.
 */
//var x = require('./x');
var y = "I am private";
var z = true;

function sum(sum1, sum2){
    return sum1 + sum2;
}
var self = module.exports = {
    someProperty: "I am public",
    addFive: function addFive(num){
        return sum(num, 5);
    },

    toggleZ: function toggleZ(num){
        console.log(num);
        console.log(z);
        return z = !z;
    }
}